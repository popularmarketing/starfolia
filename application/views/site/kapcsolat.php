<?php include('header.php');?>
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-main template-clear-fix">
						
						<!-- Features list -->
						<div class="template-component-feature-list template-component-feature-list-position-left template-clear-fix">
							
							<!-- Layout 33x33x33% -->
							<ul class="template-layout-33x33x33 template-clear-fix">
								
								<!-- Left column -->
								<li class="template-layout-column-left">
									<span class="template-icon-feature-phone-circle"></span>
									<h5>Hívjon minket</h5>
									<p>
										<?php echo $beallitasok->mobil;?>
									</p>
								</li>
								
								<!-- Center column -->
								<li class="template-layout-column-center">
									<span class="template-icon-feature-location-map"></span>
									<h5>Üzleteink</h5>
									<p>
										<?php echo $beallitasok->uzletcim?>
									</p>
								</li>
								
								<!-- Right column -->
								<li class="template-layout-column-right">
									<span class="template-icon-feature-clock"></span>
									<h5>Nyitvatartás</h5>
									<p>
										<?php echo $beallitasok->nyitvatartas;?>
									</p>
								</li>
								
							</ul>
							
						</div>
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-padding-reset template-main template-clear-fix">
						
						<!-- Contact form -->
						<form name="contact-form" id="contact-form" method="POST" action="oldal/sendmail" class="template-component-contact-form">
							
							<!-- Layout 50x50% -->
							<ul class="template-layout-50x50 template-layout-margin-reset template-clear-fix">
								
								<!-- Left column -->
								<li class="template-layout-column-left template-margin-bottom-reset">
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-name">Név *</label>
										<input type="text" name="senderName" id="contact-form-name"/>
									</div>
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-email">E-mail cím *</label>
										<input type="text" name="email" id="contact-form-email"/>
									</div>
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-phone">Tárgy *</label>
										<input type="text" name="subject" id="contact-form-phone"/>
									</div>
								</li>
								
								<!-- Right column -->
								<li class="template-layout-column-right template-margin-bottom-reset">
									<div class="template-component-form-field template-state-block">
										<label for="contact-form-message">Üzenet *</label>
										<textarea rows="1" cols="1" name="message" id="contact-form-message"></textarea>
									</div>
								</li>
								
							</ul>
							
							<!-- Button -->
							<div class="template-align-center template-clear-fix template-margin-top-1">
								<span class="template-state-block">
									<input type="submit" value="Üzenet Küldése" class="template-component-button" name="contact-form-submit" id="contact-form-submit"/>
								</span>
							</div>
							
						</form>
						
						<!-- Space -->
						<div class="template-component-space template-component-space-4"></div>
						
					</div>
					
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>				
					
					</div>
					
				</div>
<?php include('footer.php');?>