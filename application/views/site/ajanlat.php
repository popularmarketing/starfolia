<div class="template-component-booking" id="template-booking">

							<form>

								<ul>
									
									<li>

										<!-- Content -->
										<div class="template-component-booking-item-content">

											<!-- Vehicle list -->
											<ul class="template-component-booking-vehicle-list">

												<!-- Vehicle -->
												<li data-id="3ajto">

													<div>

														<!-- Icon -->
														<div class="template-icon-vehicle-small-car"></div>					

														<!-- Name -->
														<div>3 ajtós autó</div>

													</div>

												</li>

												<!-- Vehicle -->
												<li data-id="45ajto">
													<div>
														<div class="template-icon-vehicle-car-mid-size"></div>					
														<div>4 és 5 ajtós autó</div>
													</div>
												</li>

												<!-- Vehicle -->
												<li data-id="kombi">
													<div>
														<div class="template-icon-vehicle-suv"></div>					
														<div>Kombi autó</div>
													</div>
												</li>

												<!-- Vehicle -->
												<li data-id="minivan">
													<div>
														<div class="template-icon-vehicle-minivan"></div>					
														<div>Kisáruszállító</div>
													</div>
												</li>
											</ul>

										</div>

									</li>											

									<li>

										<!-- Content -->
										<div class="template-component-booking-item-content">

											<!-- Package list -->
											<ul class="template-component-booking-package-list">

												<!-- Package -->
												<?php foreach($termekek->result() as $row){?>
													<li data-id="<?php echo $row->nev?>" data-id-vehicle-rel="<?php switch($row->kategoria){
														case 1:
															echo "3ajto";
															break;
														case 2:
															echo "45ajto";
															break;
														case 3:
															echo "kombi";
															break;
														case 4:
															echo "minivan";
															break;
														default:
															echo "3ajto";
															break;
													}?>">

														<!-- Header -->
														<h4 class="template-component-booking-package-name"><?php echo $row->nev?></h4>

														<!-- Price -->
														<div class="template-component-booking-package-price">
															<span class="template-component-booking-package-price-total"><?php echo $row->ar?>Ft</span>
														</div>

														<!-- Services -->
														<ul class="template-component-booking-package-service-list">
															<li><?php echo $row->leiras?></li>
														</ul>

														<!-- Button -->
														<div class="template-component-button-box">
															<a href="kapcsolat" class="template-component-button">Hívjon minket</a>
														</div>

													</li>
												<?php }?>
											</ul>

										</div>	

									</li>
														
								</ul>

							</form>

						</div>