<?php include('header.php');?>
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Épületfóliázás</h2>
							<div></div>
							<span>Miért jó az épületfóliázás?</span>
						</div>		
						
						<!-- Text -->
						<div class="template-align-center"> 
							<p>
								
A nagy üvegfelülettel rendelkezõ szerkezetek speciális tulajdonságai miatt az épületek hõterhelése az erõsen napos idõszakokban nagy mértékben megnövekszik. A hûtésre fordított többlet energia jelentõsen megnöveli az épületek üzemeltetési költségét, továbbá a hûtõrendszer sok esetben nem képes a kívánt hõmérsékletre lehûteni az épületet. 
							</p>
							<p>
							Az üvegfelületeken keresztül a napsugárzással az épületbe bejutó hõ mennyiségét a Hõ- és Fényvédõ ablaküveg-fóliák akár 75%-kal csökkentik, így a légkondicionáló berendezések kisebb igénybevétele miatt jelentõsen csökkenthetõk a klimatizációs költségek. 
							</p>
						</div>
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">
					
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-align-center">

								<!--- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Hővédelem</h2>
									<div></div>
									<span>Fóliázás, mely véd a hőtől</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									Az egészségünkre is káros fényösszetevõ elemek - az ultraibolya sugarak - felelõsek foként a kárpitok és a berendezések kifakulásáért. Az általunk telepítet ablakfóliák az UV-sugárzást 99%-ban kiszûrik. A klimatizált helyiségekben is gyakran elviselhetetlen a napsugárzás ablak mellett érzékelt égetõ, vakító hatása.


A megfelelõ típusú ablakfólia kiválasztásával szabályozható az épületbe bejutó természetes fény erõssége és minõsége. A vakító ragyogást akár 80%-ban is kiszûri, míg a látható fényt beengedve természetes megvilágítást biztosít, javítva ezzel a munkahelyek ergonómiáját.


A fóliák az EU szabványnak megfelelõen kiszûrik a vakító fényt, így a monitorok és TV készülékek képernyõjét mentesítik a kellemetlen fényreflexiótól.
Léteznek kültéri hõvédõ fóliák,amikor a kérdéses ablak belülrõl nem közelíthetõ meg alkalmazásuk akkor javasolt.A kültéri fóliák jól ellenállnak a természeti hatásoknak,és erõs karcálló réteggel vannak bevonva,és dörzsöléssel szemben extra ellenállósággal rendelkezik. 
<br/> 
Hõvédõ épületfóliák már 5.200 Ft-tól a Starfolia kínálatában!
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

							<!-- Right column -->
							<div class="template-background-image template-background-image-hovedelemepulet"></div>

						</div>
						
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-betoresvedelem"></div>

							<!-- Right column -->
							<div class="template-align-center">
								
								<!-- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Betörésvédelem</h2>
									<div></div>
									<span>Lelassítja a betörőket</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									A  biztonsági fóliák költséghatékony és gyors megoldást nyújtanak a meglévõ üvegezett nyílászárók biztonságosabbá tételére. A napi munkamenet csupán minimális zavarása mellett felszerelhetõk.
A legtöbb  biztonsági fólia 10 éves garanciával rendelkezik és az ipar legerõsebb karcolásvédõ rétegével van ellátva. Láthatatlan védelmet nyújtanak (víztiszta esetén), és mivel a nyílászáró belsõ üvegfelületére van felhelyezve lehetetlen hozzáférni ,ezáltal beszakítani. Akrill tartalmú ragasztója erõsen az üvegfelületen tartja, így szinte csak fejszével lehet beszakítani,ez jelentõsen megnöveli a bejutás idejét,vagy teljesen valószinûtlenné teszi azt.


A biztosítók a 100 micron vastagságú,vagy annál vastagabb biztonsági fóliát ráccsal egyenértékû mechanikai védelemnek fogadják el, így csökken a kockázati tényezõ és a biztosítási díj is. Nem utolsó sorban a biztonsági fólia esztétikusabb megoldás a rácsnál.  Jelentõs a biztonsági fóliák szilánkmentesítõ funkciója is,amely nagy védelmet nyújt az összetört üveg balesetveszélyessége ellen. Pl. egy óvodában,iskolában,ahol gyakori az üvegtörés katasztrófához,súlyos sérüléseket okozhat egy összetört üvegtábla. A biztonsági fóliázás megoldja ezt a problémát, mert egyben tartja az összetört üveget, megvédve ezáltal a rajta átszaladó gyermeket, levéve ezzel a késöbbi felelõsségrevonást az intézmény vezetõje válláról.


Létezik hõvédõ biztonsági fólia is, amely ötvözi a biztonsági fólia erõsségét és a hõvédõ fólia által teremtett komfortérzetet. Az enyhe szinezésû, úgynevezett kirakatfólia pedig 99%-os UV szûréssel rendelkezik, beengedi a fényt és be is lehet rajta látni, ezáltal kirakatokra kitûnõen alkalmas, nem fakulnak mögötte a ruhák és az egyéb áucikkek.


<br/>A Starfolia kínálatában a biztonsági fóliák már 4.900 Ft-tól elérhetõek!  
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

						</div>
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-align-center">

								<!--- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Belátásvédelem</h2>
									<div></div>
									<span>Áttetsző fólia</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									A hagyományos üvegezésnek azzal együtt, hogy beengedi a helyiségekbe a napfényt, megvan az a hátránya is, hogy beláthatnak rajta akkor is amikor mi azt nem szeretnénk.
A  reflexiós(tükrös) fóliákkal könnyen orvosolhatjuk ezt a problémát.




A belátásvédelemnek általában az az alapja, hogy a betekintéstõl védeni kívánt helyiségben kevesebb fénynek kell lennie, mint ahonnan nem szeretnénk, hogy belássanak. Kellõen nagy fénykülönbség esetén mérsékelt reflexiós tényezõje következtében a tiszta üveg is megfelelõ erre a célra.




Fóliákkal úgy javíthatunk a helyzeten, hogy a szemlélõ oldaláról tükrösebbé tesszük az üveget, ill. ha ez még nem elég a belsõ oldalon csökkentjük az üveg reflexióját.
Ez természetesen nem jelent két réteg fóliát, speciális többrétegû fóliával megoldható. Amennyiben nincs elég nagy fénykülönbség az üveg két oldala között, esetleg azonosak a fényviszonyok, az egy oldalról való belátást nem megoldható. Ez esetben a belátásvédelmet matt tejfehér, homokfúvott üveghez hasonló fóliával lehet elérni, ami a fény beengedése mellet véd a betekintéstõl, de sajnos a kilátást is megakadályozza.


								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

							<!-- Right column -->
							<div class="template-background-image template-background-image-belatasvedelem"></div>

						</div>
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-dekoraciosfolia"></div>

							<!-- Right column -->
							<div class="template-align-center">
								
								<!-- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Dekorációs fóliák</h2>
									<div></div>
									<span>A jobb kinézet érdekében</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									
Teremtsen ízlésének megfelelõ hangulatot otthonában vagy irodájában. A dekorációs fóliát olyan helyre ajánljuk ahol nem - vagy nehezen megoldható a függönnyel való dekorálás (fürdõszoba,világítóablak,WC).


Tökéletes olyan helyekre is ahol szeretnénk meggátolni a belátást de csak részben (pl. fogorvosi rendelõben,kozmetikai szalonban) ilyenkor egy csíkos vagy kockás dekor megoldást jelenthet, és a helységet is feldobja.


Több fólia egymásra ragasztásával különbözõ színek és minták érhetõk el, ennek már csak a képzelet szab határt, cégünk munkatársai készséggel állnak rendelkezére a különleges ötletek kivitelezésében is.  
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

						</div>
					</div>
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					

					</div>
				</div>
				
<?php include('footer.php');?>