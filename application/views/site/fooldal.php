<?php include('header.php');?>
<?php include('primari.php');?>				

				<!-- Content -->
				<div class="template-content">

					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Kik vagyunk mi?</h2>
							<div></div>
							<span>Autó- és épület fóliázás</span>
						</div>	
						
						<!-- Layout 33x66% -->
						<div class="template-layout-33x66 template-clear-fix">
							
							<!-- Left column -->
							<div class="template-layout-column-left">
								<img src="media/image/sample/460x678/image_02.jpg" alt=""/>
							</div>
							
							<!-- Right column -->
							<div class="template-layout-column-right">
								
								<!-- Text -->
								<p class="template-padding-reset">
									<?php echo $oldal->tartalom;?>
								</p>
																
								<!-- Feature list -->
								<div class="template-component-feature-list template-component-feature-list-position-top template-clear-fix">
									
									<!-- Layout 50x50% -->
									<ul class="template-layout-50x50 template-clear-fix">
										
										<!-- Left column -->
										<li class="template-layout-column-left template-margin-bottom-reset">
											<div class="template-component-space template-component-space-2"></div>
											<span class="template-icon-feature-water-drop"></span>
											<h5>Autófóliázás</h5>
											<ul class="template-component-list">
												<li><span class="template-icon-meta-check"></span>Kiküszöbölhetõ a napsugárzásból adódó égetõ és vakító érzés</li>
												<li><span class="template-icon-meta-check"></span>Csökkenti az energiaköltséget (akár 20%-kal)</li>
												<li><span class="template-icon-meta-check"></span>Esztétikus megjelenést biztosít</li>
												<li><span class="template-icon-meta-check"></span>Biztonságot nyújt üvegtörés esetén</li>
												<li><span class="template-icon-meta-check"></span>Véd a káros UV sugárzástól</li>
											</ul>
										</li>
										
										<!-- Right column -->
										<li class="template-layout-column-right template-margin-bottom-reset">
											<div class="template-component-space template-component-space-2"></div>
											<span class="template-icon-feature-user-chat"></span>
											<h5>Kapcsolatfelvétel</h5>
											<ul class="template-component-list">
												<li><span class="template-icon-meta-check"></span>Egy nyitott és könnyen elérhető cég vagyunk</li>
												<li><span class="template-icon-meta-check"></span>Munkavégzés az ország egész területén, rövid határidõvel, garanciával, magas minõségben, mérsékelt áron</li>
												<li><span class="template-icon-meta-check"></span>A telefonszámunk non-stop hívható</li>
												<li><span class="template-icon-meta-check"></span>Ingyenes felmérés és szaktanácsadás</li>
											</ul>											
										</li>
										
									</ul>
									
								</div>
								
							</div>
								
						</div>
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix template-background-color-1">
						
						<!-- Call to action -->
						<div class="template-component-call-to-action">
							<div class="template-main">
								<h3>Első számú Autó- és Épületfóliázó a piacon</h3>
								<a href="kapcsolat" class="template-component-button">Vegye fel a kapcsolatot</a>
							</div>
						</div>
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-background-image template-background-image-5 template-background-image-parallax template-color-white template-clear-fix">
						
						<!-- Mian -->
						<div class="template-main">

							<!-- Header + subheader -->
							<div class="template-component-header-subheader">
								<h2>A folyamat</h2>
								<div></div>
								<span>Tudjuk, hogy az Ön ideje értékes</span>
							</div>	

							<!-- Space -->							
							<div class="template-component-space template-component-space-1"></div>
							
							<!-- Process list -->							
							<div class="template-component-process-list template-clear-fix">
								
								<!-- Layout 25x25x25x25% -->
								<ul class="template-layout-25x25x25x25 template-clear-fix template-layout-margin-reset">
									
									<!-- Left column -->
									<li class="template-layout-column-left">
										<span class="template-icon-feature-check"></span>
										<h5>1. Rendelés</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>
									
									<!-- Center left column -->
									<li class="template-layout-column-center-left">
										<span class="template-icon-feature-car-check"></span>
										<h5>2. Felmérés</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>
									
									<!-- Center right column -->
									<li class="template-layout-column-center-right">
										<span class="template-icon-feature-payment"></span>
										<h5>3. Értékelés</h5>
										<span class="template-icon-meta-arrow-large-rl"></span>
									</li>
									
									<!-- Right column -->
									<li class="template-layout-column-right">
										<span class="template-icon-feature-vacuum-cleaner"></span>
										<h5>4. Befejezés</h5>
									</li>
									
								</ul>
								
							</div>
							
						</div>
							
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Autófóliázás csomagok</h2>
							<div></div>
							<span>Melyikre van Önnek szüksége?</span>
						</div>	
						
						<!-- Booking -->
						<?php include('ajanlat.php');?>
						
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								$('#template-booking').booking();
							});
						</script>
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">
						
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-1"></div>

							<!-- Right column -->
							<div class="template-section-padding-1">
								
								<!-- Features list -->
								<div class="template-component-feature-list template-component-feature-list-position-top">
									
									<!-- Layout 50x50% -->
									<ul class="template-layout-50x50 template-clear-fix">
										
										<!-- Left column -->
										<li class="template-layout-column-left">
											<span class="template-icon-feature-location-map"></span>
											<h5>Elérhetőség</h5>
											<p>Munkavégzés az ország egész területén, rövid határidõvel, garanciával, magas minõségben, mérsékelt áron.</p>
										</li>
										
										<!-- Right column -->
										<li class="template-layout-column-right">
											<span class="template-icon-feature-eco-nature"></span>
											<h5>Ingyenes felmérés</h5>
											<p>Pest- és Komárom-Esztergom megye területén díjtalan felmérést ajánlunk. Megtekintheti mintakollekciónkat.</p>											
										</li>
										
										<!-- Left column -->
										<li class="template-layout-column-left">
											<span class="template-icon-feature-team"></span>
											<h5>Tapasztalt csapat</h5>
											<p>Csapatunknak több mint 8 év tapasztalata van Autó és Épület, hő- és biztonságifóliázás munkákban.</p>											
										</li>
										
										<!-- Right column -->
										<li class="template-layout-column-right">
											<span class="template-icon-feature-spray-bottle"></span>
											<h5>Jó Minőség</h5>
											<p>Amerikai gyártmányú fóliákat használunk, ami nem csak árban, de minőségben is a piac első helyére tesz minket.</p>											
										</li>
										
									</ul>
									
								</div>

							</div>

						</div>
						
					</div>
					
					<!-- Section -->
					<div class="template-section template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Legújabb munkáink</h2>
							<div></div>
							<span>Autófóliázás galéria</span>
						</div>	
						
						<!-- Gallery -->
						<div class="template-component-gallery">

							<!-- Images list -->
							<ul class="template-component-gallery-image-list">
								<?php foreach($kepek->result() as $row){?>
									<!-- Image -->
									<li class="template-filter-hand-wash">

										<div class="template-component-image template-component-image-preloader">

											<!-- Orginal image -->
											<a href="assets/uploads/files/<?php echo $row->file;?>" class="template-fancybox" data-fancybox-group="gallery-1">

												<!-- Thumbnail -->
												<img src="assets/uploads/files/<?php echo $row->file;?>" alt=""/>

												<!-- Image hover -->
												<span class="template-component-image-hover">
													<span>
														<span>
															<span class="template-component-image-hover-header"><?php echo $row->nev;?></span>
														</span>
													</span>
												</span>
											</a>
										</div>

									</li>
								<?php }?>

							</ul>

						</div>	
						
						<!-- Button -->
						<div class="template-align-center">
							<a href="galeria" class="template-component-button">Tovább a galériához</a>
						</div>
						
					</div>		
					
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Features list -->
						<div class="template-component-feature-list template-component-feature-list-position-left template-clear-fix">
							
							<!-- Layout 33x33x33% -->
							<ul class="template-layout-33x33x33 template-clear-fix">
								
								<!-- Left column -->
								<li class="template-layout-column-left">
									<span class="template-icon-feature-phone-circle"></span>
									<h5>Hívjon minket</h5>
									<p>
										<?php echo $beallitasok->vezetekes?><br/>
										<?php echo $beallitasok->mobil?><br/>
									</p>
								</li>
								
								<!-- Center column -->
								<li class="template-layout-column-center">
									<span class="template-icon-feature-location-map"></span>
									<h5>Címeink</h5>
									<p>
										<?php echo $beallitasok->uzletcim?>
									</p>
								</li>
								
								<!-- Right column -->
								<li class="template-layout-column-right">
									<span class="template-icon-feature-clock"></span>
									<h5>Nyitvatartás</h5>
									<?php print_r($beallitasok->nyitvatartas)?>
								</li>
								
							</ul>
						</div>
						
					</div>
					
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					
					
					</div>
					
				</div>
				
				<!-- Footer -->
				<?php include('footer.php');?>