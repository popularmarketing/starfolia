<div class="template-footer">
					
					<div class="template-main">
						
						<!-- Footer top -->
						<div class="template-footer-top">
							
							<!-- Layout 25x25x25x25 -->
							<div class="template-layout-25x25x25x25 template-clear-fix">
								
								<!-- Left column -->
								<div class="template-layout-column-left">
									<h6>Rólunk</h6>
									<p>Autók és épületek biztonsági és hővédő fóliázása, 8 év tapasztalattal, amerikai gyártmányú fóliákkal!<br/>
Vállaljuk egyéni vállalkozók és társaságok teljeskörû könyvelését, délutáni nyitvatartással!


Bõvebb információ: 06 30/ 462 5163</p>
									<img src="assets/uploads/files/<?php echo $beallitasok->logo;?>" alt="" class="template-logo" />
								</div>

								<!-- Center left column -->
								<div class="template-layout-column-center-left">
									<h6>Szolgáltatások</h6>
									<ul class="template-list-reset">
										<li><a href="autofoliazas"Autó fóliázás</a></li>
										<li><a href="epuletfoliazas">Épület fóliázás</a></li>
										<li><a href="fenyezesvedelem">Fényezés védelem</a></li>
										<li><a href="carbonfolia">Carbon fólia</a></li>
										<li><a href="konyveles">Könyvelés</a></li>
									</ul>									
								</div>
								
								<!-- Center right column -->
								<div class="template-layout-column-center-right">
									<h6>Cégünk</h6>
									<ul class="template-list-reset">
										<li><a href="galeria">Galéria</a></li>
										<li><a href="szolgaltatasok">Szolgáltatásaink</a></li>
										<li><a href="arlista">Árlista</a></li>
										<li><a href="kapcsolat">Kapcsolat</a></li>
									</ul>
								</div>
								
								<!-- Right column -->
								<div class="template-layout-column-right">
									<h6>Ingyenes felmérés</h6>
									<p>Pontos méretek hiányában, vagy ha nem áll módjában lemérni a felületeket, kérje munkatársunk Pest- és Komárom-Esztergom megye területén díjtalan felmérését és tekintse meg  mintakollekciónkat.</p>
								</div>
								
							</div>
							
						</div>
						
						<!-- Footer bottom -->
						<div class="template-footer-bottom">
							
							<!-- Social icon list -->
							<ul class="template-component-social-icon-list template-component-social-icon-list-2">
								<li><a href="#.html" class="template-icon-social-twitter"></a></li>
								<li><a href="#.html" class="template-icon-social-facebook"></a></li>
								<li><a href="#.html" class="template-icon-social-dribbble"></a></li>
								<li><a href="#.html" class="template-icon-social-envato"></a></li>
								<li><a href="#.html" class="template-icon-social-behance"></a></li>
								<li><a href="#.html" class="template-icon-social-youtube"></a></li>
							</ul>
							
							<!-- Copyright -->
							<div class="template-footer-bottom-copyright">
								Készítette <a href="http://popularmarketing.hu" target="_blank"><img src="media/image/popularmarketing-logo.png" style="display:inline; height:50px"></a> &copy; 2016 - Minden jog fenntartva
							</div>
							
						</div>
						
					</div>

				</div>

				<!-- Search box -->
				<div class="template-component-search-form">
					<div></div>
					<form>
						<div>
							<input type="search" name="search"/>
							<span class="template-icon-meta-search"></span>
							<input type="submit" name="submit" value=""/>
						</div>
					</form>
				</div>
				
				<!-- Go to top button -->
				<a href="#go-to-top" class="template-component-go-to-top template-icon-meta-arrow-large-tb"></a>
				
				<!-- Wrapper for date picker -->
				<div id="dtBox"></div>
				
				<!-- JS files -->
				<script type="text/javascript" src="script/jquery-ui.min.js"></script>
				<script type="text/javascript" src="script/superfish.min.js"></script>
				<script type="text/javascript" src="script/jquery.easing.js"></script>
				<script type="text/javascript" src="script/jquery.blockUI.js"></script>
				<script type="text/javascript" src="script/jquery.qtip.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox.js"></script>
				<script type="text/javascript" src="script/isotope.pkgd.min.js"></script>
				<script type="text/javascript" src="script/jquery.actual.min.js"></script>
				<script type="text/javascript" src="script/jquery.flexnav.min.js"></script>
				<script type="text/javascript" src="script/jquery.waypoints.min.js"></script>
				<script type="text/javascript" src="script/sticky.min.js"></script>
				<script type="text/javascript" src="script/jquery.scrollTo.min.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-media.js"></script>
				<script type="text/javascript" src="script/jquery.fancybox-buttons.js"></script>
				<script type="text/javascript" src="script/jquery.carouFredSel.packed.js"></script>
				<script type="text/javascript" src="script/jquery.responsiveElement.js"></script>
				<script type="text/javascript" src="script/jquery.touchSwipe.min.js"></script>
				<script type="text/javascript" src="script/DateTimePicker.min.js"></script>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
				
				<!-- Revolution Slider files -->
				<script type="text/javascript" src="script/revolution/jquery.themepunch.revolution.min.js"></script>
				<script type="text/javascript" src="script/revolution/jquery.themepunch.tools.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.actions.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.carousel.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.kenburn.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.migration.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.navigation.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.parallax.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.slideanims.min.js"></script>
				<script type="text/javascript" src="script/revolution/extensions/revolution.extension.video.min.js"></script>

				<!-- Plugins files -->
				<script type="text/javascript" src="plugin/booking/jquery.booking.js"></script>
				
				<!-- Components files -->
				<script type="text/javascript" src="script/template/jquery.template.tab.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.image.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.helper.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.header.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.counter.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.gallery.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.goToTop.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.fancybox.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.moreLess.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.googleMap.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.accordion.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.searchForm.js"></script>
				<script type="text/javascript" src="script/template/jquery.template.testimonial.js"></script>
				<script type="text/javascript" src="script/public.js"></script>

			</body>
			
		</html>