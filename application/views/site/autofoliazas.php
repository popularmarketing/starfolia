<?php include('header.php');?>
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Autó fóliázás</h2>
							<div></div>
							<span>Miért jó az autófólia?</span>
						</div>		
						
						<!-- Text -->
						<div class="template-align-center"> 
							<p>
								
Az autófólia a gépkocsinak esztétikusabb, sportosabb megjelenést biztosít,de nem ez a fóliák egyetlen elõnye.
A sötétebb árnyalatú és erõsebb fényvédõ fólia a gépjármûbe való belátást is korlátozza, míg belülrõl megfelelõ kilátást biztosít.
							</p>
							<p>
							A fóliák reflexiós napvédõ réteget képeznek az üveg belsõ felületén.
Típustól függõen a napsugárzásból származó hõ nagy részét kinn tartják (akár 55%) míg a látható fényt beengedik.
							</p>
							<p>
							A szabadalmaztatott kristálytiszta ragasztó nagyon erõs tapadást biztosít, így az ablakra felragasztott fólia garantáltan nem hólyagosodik fel, nem repedezik, nem ráncosodik és nem válik el az üvegtõl.Vannak nagyon világos,alig észrevehetõ szinezésû hõvédõ fóliák is,melyeknek nagyon erõs a hõkinntartó hatása,nem fontos tehet erõsen leárnyékolnia autóját a hatékony hõvédelemhez.
							</p>
						</div>
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">
					
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-align-center">

								<!--- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Hővédelem</h2>
									<div></div>
									<span>Fóliázás, mely véd a hőtől</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									Az egyre erõsödõ felmelegedés és a napsugárzás miatt sokszor az autónkban sem érezzük jól magunkat,hiába a lékondicionáló,ha az arcunkat égeti a nap.
Az üvegen átáramló erõs meleg miatt egyre nagyobb fokozatra kapcsoljuk a
légkondicionálást, ennek következményeként pedig inkább csak megfázunk
mintsem a komfort érzetünkön javítanánk.
Az autófólia használata során csökken az autónk belsõ légterének a felmelegedése, kizárva az égetõ UV sugárzást és az alacsonyabb fokozatra állított klímaberendezés miatt közvetve bár, de az autófóliázásnak köszönhetõen még az autónk fogyasztása is csökken. 
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="arlista" class="template-component-button">Érdekel</a>
								
							</div>

							<!-- Right column -->
							<div class="template-background-image template-background-image-hovedelem"></div>

						</div>
						
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-fenyvedelem"></div>

							<!-- Right column -->
							<div class="template-align-center">
								
								<!-- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Fényvédelem</h2>
									<div></div>
									<span>Fóliázás, mely sötétít</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									Az árnyékolás a másik fontos szempont az autófóliák választásánál.
A vakító napfény is zavaróan hat ránk mindennapi autóvezetésünk során.
Alacsony fényáteresztéssel rendelkezõ fóliákat alkalmazunk ennek enyhítésére.
Áruvédelmi szempontból a kis haszonjármûvek esetén elengedhetetlen a raktér belátás elleni védelme, amelyekre a legsötétebb anyagokat javasoljuk.  
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="arlista" class="template-component-button">Érdekel</a>
								
							</div>

						</div>
						
					</div>
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					

					</div>
				</div>
				
<?php include('footer.php');?>