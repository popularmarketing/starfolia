<?php include('header.php')?>				
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">
					
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-clear-fix">

							<!-- Left column -->
							<div class="template-align-center">
								
								<h1>404</h1>
								
								<h3 class="template-margin-top-2">Oldal nem találva.</h3>
								
								<p class="template-padding-reset template-margin-top-2">
									Mindenhol megnéztük, de a kért oldal nem létezik.<br/>
									<a href="<?php echo base_url();?>">Ide klikkelve</a> térhet vissza a főoldalra.
								</p>			
							</div>

							<!-- Right column -->
							<div class="template-background-image template-background-image-3"></div>
							
						</div>
						
					</div>
					
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Show Map</span>
								<span class="template-component-google-map-button-label-hide">Hide Map</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					
					
					</div>

				</div>
				
				<?php include('footer.php')?>