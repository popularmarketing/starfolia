<?php include('header.php');?>
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-main template-align-center">
						
						<!-- Gallery -->
						<div class="template-component-gallery">

							<!-- Filter buttons list -->
							<ul class="template-component-gallery-filter-list">
								<li><a href="#" class="template-filter-autofoliazas template-state-selected">Autók foliázása</a></li>
								<li><a href="#" class="template-filter-carbonfoliazas">Carbon fóliázás</a></li>
								<li><a href="#" class="template-filter-epuletfoliazas">Épületek fóliázása</a></li>
							</ul>

							<!-- Images list -->
							<ul class="template-component-gallery-image-list">

							
							
							<?php foreach($kepek->result() as $row){?>
								<!-- Image -->
								<li class="template-filter-<?php echo $row->image_url;?>">
									<div class="template-component-image template-component-image-preloader">

										<!-- Orginal image -->
										<a href="assets/uploads/files/<?php echo $row->file;?>" class="template-fancybox" data-fancybox-group="gallery-1">

											<!-- Thumbnail -->
											<img src="assets/uploads/files/<?php echo $row->file;?>" alt=""/>

											<!-- Image hover -->
											<span class="template-component-image-hover">
											</span>
										</a>
									</div>

								</li>
								
							<?php }?>
								
							</ul>

						</div>					
					
					</div>
					
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					
					
					</div>
					
				</div>
<?php include('footer.php');?>