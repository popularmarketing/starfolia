<div class="template-header template-header-background template-header-background-<?php echo rand(1,3)?>">

					<!-- Top header -->
					<div class="template-header-top">

						<!-- Logo -->
						<div class="template-header-top-logo">
							<a href="index.html" title="">
								<img src="media/image/logo.png" alt="" class="template-logo"/>
								<img src="media/image/logo_sticky.png" alt="" class="template-logo template-logo-sticky"/>
							</a>
						</div>

						<!-- Menu-->
						<div class="template-header-top-menu template-main">
							<nav>
							
								<div class="template-component-menu-default">
									<ul class="sf-menu">
										<li><a href="">Főoldal</a></li>
										<li>
											<a href="autofoliazas">Autófóliázás</a>
											<ul>
												<li><a href="arlista">Árlista</a></li>
											</ul>
										</li>
										<li>
											<a href="epuletfoliazas">Épületfóliázás</a>										
										</li>
										<li>
											<a href="szolgaltatasok">Egyéb szolgáltatások</a>
										</li>
										<li><a href="akciok">Akciók</a></li>
										<li><a href="galeria">Galéria</a></li>
										<li>
											<a href="kapcsolat">Kapcsolat</a>
										</li>
									</ul>
								</div>
								
							</nav>
							<nav>
							
								<!-- Mobile menu-->
								<!-- Mobile menu-->
								<div class="template-component-menu-responsive">
									<ul class="flexnav">
										<li><a href="#"><span class="touch-button template-icon-meta-arrow-large-tb template-component-menu-button-close"></span>&nbsp;</a></li>
										<li><a href="">Főoldal</a></li>
										<li>
											<a href="autofoliazas">Autófóliázás</a>
											<ul>
												<li><a href="arlista">Árlista</a></li>
											</ul>
										</li>
										<li>
											<a href="epuletfoliazas">Épületfóliázás</a>										
										</li>
										<li>
											<a href="szolgaltatasok">Egyéb szolgáltatások</a>
										</li>
										<li><a href="akciok">Akciók</a></li>
										<li><a href="galeria">Galéria</a></li>
										<li>
											<a href="kapcsolat">Kapcsolat</a>
										</li>
									</ul>							
								</div>
								
							</nav>
							<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									$('.template-header-top').templateHeader();
								});
							</script>
						</div>

						<!-- Social icons -->
						<div class="template-header-top-icon-list template-component-social-icon-list-1">
							<ul class="template-component-social-icon-list">
								<li><a href="#.html" class="template-icon-social-twitter" target="_blank"></a></li>
								<li><a href="#.html" class="template-icon-social-facebook" target="_blank"></a></li>
								<li><a href="kapcsolat" class="template-icon-meta-cart"></a></li>
								<li><a href="#.html" class="template-icon-meta-menu"></a></li>
							</ul>
						</div>

					</div>				
		
					<div class="template-header-bottom">
					
						<div class="template-main">

							<div class="template-header-bottom-page-title">
								<h1><?php echo $oldal->nev;?></h1>
							</div>

							<div class="template-header-bottom-page-breadcrumb">
								<a href="">Főoldal</a><span class="template-icon-meta-arrow-right-12"></span><a href="<?php echo $oldal->url;?>"><?php echo $oldal->nev;?></a>
							</div>
						
						</div>

					</div>
						
				</div>