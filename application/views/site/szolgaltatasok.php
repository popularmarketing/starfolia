<?php include('header.php');?>
<?php include('primari2.php');?>
				<!-- Content -->
				<div class="template-content">
					
					<!-- Section -->
					<div class="template-section template-section-padding-1 template-clear-fix template-main">
						
						<!-- Header + subheader -->
						<div class="template-component-header-subheader">
							<h2>Egyéb szolgáltatásaink</h2>
							<div></div>
							<span></span>
						</div>		
						
						<!-- Text -->
						<div class="template-align-center"> 
							<p>
							
   

							</p>
						</div>
					</div>
					
					<!-- Section -->
					<div class="template-section template-section-padding-reset template-clear-fix">
					
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-align-center">

								<!--- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Fényezés védelem</h2>
									<div></div>
									<span>A sérülékeny karosszéria elemek számára</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									 Megvédi a karosszériát az apróbb kõfelverõdésektõl, a faágak okozta karcolásoktól és a bogarak által elõidézett sérülésektõl.


    100%-ig átlátszó, rendkívül rugalmas.


    5 év garancia a biztosíték arra, hogy a fólia az évek során nem színezõdik el, nem keményedik ki, és nem hólyagosodik fel


    Könnyen telepíthetõ (kb. 3 óra alatt készül el egy autó karosszéiravédõ fóliázása), és könnyen eltávolítható


    A fóliával fedett területeket ugyanúgy lehet tisztítani, mint az autó többi részét


    Sõt, a fóliázott felület továbbra is polírozható


    A Paint-Grad UV stabil, vagyis átengedi az UV sugarakat, hogy az autó többi részéhez hasonlóan változhasson a fóliával fedett felület színe


    Az autótípusok 98%-ára rendelhetõek számítógépes technológiával készített minták, amelyek elsõsorban a kõfelvelverõdés szemponjából különösen veszélyeztetett részeket fedik, pl. lökhárítót, motorháztetõt, fényszórókat, tükröket, sárhányókat, a hátsó lökhárító pakoló részét, stb.
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

							<!-- Right column -->
							<div class="template-background-image template-background-image-fenyezesvedelem"></div>

						</div>
						
						<!-- Flex layout 50x50% -->
						<div class="template-layout-flex template-background-color-1 template-clear-fix">

							<!-- Left column -->
							<div class="template-background-image template-background-image-carbonfolia"></div>

							<!-- Right column -->
							<div class="template-align-center">
								
								<!-- Header + subheader -->
								<div class="template-component-header-subheader">
									<h2>Carbon fólia</h2>
									<div></div>
									<span>3D karbonfólia  az autófóliázás forradalma!</span>
								</div>
								
								<!-- Text -->
								<p class="template-padding-reset">
									A Carbon fóliák öntött szerkezetük miatt tökéletesen alkalmasak gépjármûvek teljes bevonására.
Nem kell attól tartani, hogy a fólia évek múltán is akár leválik az ívekrõl. A tökéletes és könnyû telepíthetõségrõl a strukturált liner gondoskodik, mely légcsatornákat váj a ragasztóba, ezzel könnyítve a levegõ kiszorítását ragasztáskor. A Carbon sorozat négy színben érhetõ el. Fekete, fehér, antracit, ezüst.

Mivel a karbonfólia öntött szerkezetû ezért bármilyen felületre felragasztható, felhasználásának csak a képzelet szab határt (pl.: laptop, aktatáska, mikrohullámú sütõ, de akár még edény is.) A fólia határtalan tervezõi és kivitelezõi kreativitást tesz lehetõvé. A karbonfólia elõnye, hogy optimális fedést és térbeli stabilitást biztosít. Az anyagvastagságból adódóan kiválóan alkalmas kisebb fényezési hibák eltüntetésére is pl.: tükör, lökhárító, autó-, motorelemek stb. A karbonfólia nem helyettesíti a karosszériavédõ fóliát még ha nyújt is bizonyos fokú védelmet.

Cégünk a tuning autókra külsõ és belsõ felhasználásra egyaránt javasolja az új Carbon fóliákat.
Kimagasló ellenálló képességgel rendelkezik (fizikai behatások, kopás, karcolás, folyamatos vízterhelés.)
A fólia gyártásánál kiemelt szempont volt, hogy bírja az erõs UV sugárzást, õrizze a színét, alakváltozás vagy leválás nélkül elviselje a szélsõséges hõmérsékletet.

A karbonfólia méltán népszerû autós-, motoros körökben. Ha szeretné Ön is autóját egyedivé, látványossá tenni, akkor Önnek a 3D karbonfólia nyújtja az ideális megoldást. 
								</p>
								
								<!-- Space -->
								<div class="template-component-space template-component-space-2"></div>
								
								<!-- Button -->
								<a href="kapcsolat" class="template-component-button">Érdekel</a>
								
							</div>

						</div>
						
						
					</div>
					<!-- Google Maps -->
					<div class="template-section template-section-padding-reset template-clear-fix">
									
						<!-- Google Map -->
						<div class="template-component-google-map">

							<!-- Content -->
							<div class="template-component-google-map-box">
								<div class="template-component-google-map-box-content"></div>
							</div>

							<!-- Button -->
							<a href="#" class="template-component-google-map-button">
								<span class="template-icon-meta-marker"></span>
								<span class="template-component-google-map-button-label-show">Térkép mutatása</span>
								<span class="template-component-google-map-button-label-hide">Térkép elrejtése</span>
							</a>

						</div>

						<script type="text/javascript">

							jQuery(document).ready(function()
							{
								jQuery('.template-component-google-map').templateGoogleMap(
								{
									coordinate		:
									{
										lat			:	'47.71458',
										lng			:	'18.73307'
									},
									dimension		:
									{
										width		:	'100%',
										height		:	'400px'
									},
									marker			:	'media/image/map_pointer.png'
								});
							});

						</script>					

					</div>
				</div>
				
<?php include('footer.php');?>